from django.shortcuts import render

# Create your views here.

from django.db import transaction

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import CreateAPIView

from . import serializers, models

# create authentication user

class CreateUserView(CreateAPIView):
    serializer_class = serializers.CreateUserProfile

    def create(self, request, *args, **kwargs):
        serializer = serializers.CreateUserSerializer(
            data=self.request.data
        )
        if not serializer.is_valid():
            return Response(
                {
                    "message": "Invalid Object",
                    "details": serializer.errors
                },
                status=status.HTTP_400_BAD_REQUEST)
        
        with transaction.atomic():
            user = models.User.objects.create_user(
                **self.request.data
            )
            model_serializer = serializers.UserModelSerializer(
                user,
                many=False,
                read_only=True
            ) 
            return Response(
                {
                    "message": "User created successfully",
                    "data": model_serializer.data
                },
                status=status.HTTP_201_CREATED)