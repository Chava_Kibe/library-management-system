from asyncio import FastChildWatcher
from tkinter import CASCADE
from django.db import models
import uuid
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
    email and password are required. Other fields are optional.
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    username = models.CharField(_("username"), max_length=20, blank=False, null=False, unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_superuser = models.BooleanField(
        _("superuser status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS= []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        # abstract = True

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name


class Book(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(_("title"),max_length=255,blank=False, null=False)
    author = models.CharField(_("author"), max_length=255, blank=False, null=False)
    isbn = models.CharField(_("ISBN"), max_length=255, unique=True, blank=False, null=False)
    upload_date=models.DateTimeField(_("date joined"), auto_now_add=True)

    def __str__(self):
        return self.title

class BookBorrowed(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    STATUS_CHOICES = (
    ('available', 'Available'),
    ('borrowed', 'Borrowed'),
    )
    book_borrower = models.ForeignKey(User, on_delete=models.CASCADE, related_name="books_borrowed")
    status = models.CharField(_("status"), max_length=50, choices=STATUS_CHOICES)
    borrowed_date = models.DateTimeField(_("date borrowed"), auto_now_add=True)
    expected_return_date = models.DateTimeField(_("expected return"), auto_now=True)

class ApprovedBook(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    STATUS_CHOICES = (
    ('approve', 'Approved'),
    ('decline', 'Declined'),
    )
    approver = models.ForeignKey(User, on_delete=models.CASCADE, related_name="books_approver")
    approved_book = models.OneToOneField(BookBorrowed, on_delete=models.CASCADE, related_name="approved_books")