from django.urls import path

from .views import CreateUserView

urlpatterns = [
    path('api/create-user', CreateUserView.as_view(), name='create_user'),
    # path('api/login', SignInView.as_view(), name='token_obtain_pair'),
    # path('api/login/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    # path('api/logout', SignOutView.as_view(), name='token_logout'),
]