from pickletools import read_long1
from pyexpat import model
# from httplib2 import Authentication
# from numpy import False_
from rest_framework import serializers
from . import models


class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name'
        ]
        read_only_fields = ["id"]

class CreateBookSerializer(serializers.Serializer):
    title = serializers.CharField()
    author = serializers.CharField()
    isbn = serializers.CharField()
    upload_date = serializers.DateField(allow_null=True)

class CreateBookBorrowedSerializer(serializers.Serializer):
    book_borrower = serializers.CharField()
    status = serializers.CharField()
    borrowed_date = serializers.DateField()
    expected_return_date = serializers.DateField()


class CreateApprovedBookSerializer(serializers.Serializer):
    approver = serializers.CharField()
    approved_book = serializers.ReadOnlyField()

class CreateUserProfile(serializers.Serializer):
    username = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    is_staff = serializers.BooleanField()
    date_joined = serializers.DateField()
    password = serializers.CharField()
    books = serializers.ListSerializer(
        child=CreateBookSerializer(),
        allow_empty=False
    )
    books_borrowed = serializers.ListSerializer(
        child=CreateApprovedBookSerializer(),
        allow_empty=False
    )
    approved_books = serializers.ListSerializer(
        child=CreateApprovedBookSerializer(),
        allow_empty=False)



class BookBorrowedSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BookBorrowed
        fields = '__all__'

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Book
        fields = '__all__'

class ApprovedBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ApprovedBook
        fields = '__all__'

class UserProfileModelSerializer(serializers.ModelSerializer):
    books = BookSerializer(many=False, read_only=True)
    books_borrowed = BookBorrowedSerializer(many=True, read_only=True)
    approved_books = ApprovedBookSerializer(many=True, read_only=True)

    class Meta:
        model=models.User
        fields = '__all__'